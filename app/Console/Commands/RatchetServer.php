<?php

namespace App\Console\Commands;

use App\Services\RatchetService;
use App\Ws\WsKernel;
use Illuminate\Console\Command;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

class RatchetServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ratchet:serve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Ratchet server';

    private $ws_kernel;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(WsKernel $ws_kernel)
    {
        parent::__construct();

        $this->ws_kernel = $ws_kernel;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $port = 8080;

        $ws_kernel = $this->ws_kernel;

        $server = IoServer::factory(
            new HttpServer(
                new WsServer($ws_kernel)
            ),
            $port
        );

        $server->loop->addPeriodicTimer(10, function() use ($server, $ws_kernel) {
            // Log open connections in terminal.
            if(env('APP_ENV') == 'local') 
                $ws_kernel->usersLog();
        });

        system('clear');
        $this->info("Server start at localhost:{$port}");

        $server->run();
    }
}
