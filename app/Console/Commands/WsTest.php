<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use WebSocket\Client;

class WsTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ratchet:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test maximum number of connections';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Check memory usage.
        $memoty_usage = $this->bitesToMegabites(memory_get_usage());
        $this->info("Start memory usage: $memoty_usage MB");

        // Check available extensions.
        $this->checkExtension();

        // Check max number of connections in current envarioment.
        $this->maxConnectionsNumber();

        // Check memory usage.
        $memoty_usage = $this->bitesToMegabites(memory_get_usage());
        $memory_peak = $this->bitesToMegabites(memory_get_peak_usage());
        $this->info("Memory usage: $memoty_usage MB");
        $this->info("Memory peak: $memory_peak MB");
    }

    /**
     * Convert bytes to Megabytes.
     * 
     * @param  Integer $bites 
     * @return Float
     */
    private function bitesToMegabites($bites)
    {
        return round($bites/(pow(1024,2)), 2);
    }

    /**
     * Check available loop extensions. 
     * We require anything better than default StreamSelectLoop to run 
     * more than 1024 open connections.
     */
    private function checkExtension()
    {
        if (function_exists('event_base_new')) {
            $this->info("We can use LibEventLoop!!");
        } elseif (class_exists('libev\EventLoop', false)) {
            $this->info("We can use LibEvLoop!!");
        } elseif (class_exists('EventBase', false)) {
            $this->info("We can use ExtEventLoop!!");
        } else {
            $this->info("We can use StreamSelectLoop");
        }
    }

    /**
     * Check how many connection can we handle.
     * Satisfactory limit is set in configuration.
     */
    private function maxConnectionNumber()
    {
        $i = 0;
        $limit = config('games.open_connections_limit');

        try {
            while ($i < $limit) {
                $client[$i] = new Client(env('WS_URL') . "?user=0&nickname=Test$i&to=0&as=TEST");
                $client[$i]->send("CON$i"); 
                $i++;
            }

            $this->info("System allows >$i connections");

        } catch(\Exception $e) {
            $this->info("System allows $i connections");
        }
    }
}
