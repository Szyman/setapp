<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = ['sheet_id', 'code', 'questions', 'current_question', 'current_step'];

    /**
     * Relationship with Sheet model.
     * @return Model
     */
    public function sheet()
    {
        return $this->belongsTo('App\Sheet');
    }

    /**
     * Relationship with PLayer model.
     * @return Model
     */
    public function players()
    {
        return $this->hasMany('App\Player');
    }
}
