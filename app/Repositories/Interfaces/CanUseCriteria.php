<?php

namespace App\Repositories\Interfaces;

interface CanUseCriteria {

    /**
     * Apply criterium in repository scope.
     * @param  Illuminate\Database\Eloquent\Model $model
     * @return Illuminate\Database\Eloquent\Model
     */
    function applyCriteria();

    /**
     * Add criteria to list.
     * @param Criteira $criteria 
     */
    function addCriteria($criteria);

    /**
     * Remove criteria from list.
     * @param  Criteira $criteria
     */
    function removeCriteria($criteria);
}
