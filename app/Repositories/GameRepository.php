<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Repositories\Interfaces\Repository;

class GameRepository extends BaseRepository implements Repository
{
	/**
	 * Return repository model.
	 * @return Model
	 */
    public function model()
    {
        return \App\Game::class;
    }
}