<?php

namespace App\Repositories\Criteria;

use App\Repositories\Interfaces\Criteria;

/**
* Criteria select only sheets tagged as a quiz.
*/
class IsQuiz implements Criteria
{
    public function apply($model)
    {
        return $model->isQuiz();
    }    
}