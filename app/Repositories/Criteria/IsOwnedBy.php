<?php

namespace App\Repositories\Criteria;

use App\Repositories\Interfaces\Criteria;

/**
* Criteria select only entries owned by passed user.
*/
class IsOwnedBy implements Criteria
{
    private $owner;

    function __construct(int $user_id)
    {
        $this->owner = $user_id;
    }

    public function apply($model)
    {
        return $model->whereUserId($this->owner);
    }    
}