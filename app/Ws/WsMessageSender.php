<?php

namespace App\Ws;

use App\Exceptions\WsException;
use Illuminate\Support\Facades\Log;

class WsMessageSender
{
    /*
    |--------------------------------------------------------------------------
    | Websocket Message Sender
    |--------------------------------------------------------------------------
    |
    | Message Sender is responsible for sending a message to 
    | passed, open connection.
    |
    */

    /**
     * Send message to connected client.
     * @param  array  $user    
     * @param  string $message 
     * @param  array  $data    
     * @return void
     */
    public static function send($user, $message, $data = [])
    {
        $array = [
            'message' => $message,
            'data' => $data
        ];

        $json = json_encode($array);

        if(! empty($user['connection'])) {
            $connection = $user['connection'];
            try {
                $connection->send($json);
            } catch(WsException $e) {
                Log::error($e);

                $connection_manager = App::make('App\Ws\WsConnectionManager');
                $connection_manager->remove($connection);
            }
        }
    }
}