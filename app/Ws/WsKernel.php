<?php

namespace App\Ws;

use App\Exceptions\WsException;
use App\Repositories\GameRedisRepository;
use App\Repositories\PlayerRedisRepository;
use App\Ws\WsRouter;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class WsKernel implements MessageComponentInterface
{
    /*
    |--------------------------------------------------------------------------
    | Websocket Kernel
    |--------------------------------------------------------------------------
    |
    | This class is a center of communication between clients and server-side logic.
    | Kernel implements all function required by Ratchet.
    |
    */

    /**
     * Websocket Router.
     * @var WsRouter
     */
    protected $router;

    /**
     * All connections in system.
     * @var collection
     */
    protected $connections;

    public function __construct(WsRouter $router)
    {
        $this->router = $router;

        $this->connection_manager = App::make('App\Ws\WsConnectionManager');
    }

    /**
     * Method called by server when new connection was open.
     * @param  ConnectionInterface $conn 
     * @return void
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->connection_manager->add($conn);
    }

    /**
     * Method called by server when message arrives.
     * @param  ConnectionInterface $conn 
     * @param  Json/String         $msg  
     * @return void
     */
    public function onMessage(ConnectionInterface $conn, $msg)
    {
        $router = $this->router;

        $router->setSender($conn);
        $router->decodeMessage($msg);

        try {
            $router->run();
        } catch(WsException $e) {
            $this->onError($conn, $e);
        }

        Log::debug($msg);
    }

    /**
     * Method called by server when connection is closing by client.
     * @param  ConnectionInterface $conn 
     * @return void
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->connection_manager->remove($conn);
    }

    /**
     * Method called by server when error appears.
     * @param  ConnectionInterface $conn 
     * @param  Exception $e 
     * @return void
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        Log::error('Connection error: ' . $e->getMessage() );

        $this->connection_manager->remove($conn);
    }
    
    /**
     * Log users in application. 
     * LOCAL ENV ONLY
     * @return void
     */
    public function usersLog()
    {
        system('clear');
        echo 'USERS' . PHP_EOL;
        echo "CONNECTION\t | USER\t | ROLE\t\t | GAME" . PHP_EOL;
        echo '---------------------------------------------------------------------' . PHP_EOL;
        foreach($this->connection_manager->all() as $connection) {
            if(! empty($connection['user_id']))
                echo "{$connection['connection']->resourceId}\t\t | {$connection['user_id']}\t | {$connection['role']}\t | {$connection['game_code']}" . PHP_EOL;
        }
        echo '---------------------------------------------------------------------' . PHP_EOL;
        echo PHP_EOL;
    }
}