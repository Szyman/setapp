<?php

namespace App\Ws;

use App\Exceptions\WsException;
use Illuminate\Support\Collection;
use Ratchet\ConnectionInterface;

class WsRouter 
{
	/*
    |--------------------------------------------------------------------------
    | Websocket Router
    |--------------------------------------------------------------------------
    |
    | Message Sender is responsible for sending a message to 
    | passed, open connection.
    |
    */

    /**
     * All connections in app.
     * @var collection
     */
    private $connections;

    /**
     * Ratchet Connection
     * @var ConnectionInterface
     */
    private $sender;

    /**
     * Message/
     * @var string
     */
    private $message;

    /**
     * Message data.
     * @var Json Object
     */
    private $message_data;

    /**
     * Available routes.
     * @var array
     */
    private $route_map;

    function __construct()
    {
        $this->route_map = include_once(base_path() . '/routes/ws.php');
    }
    
    /**
     * Store connection of sender.
     * @param ConnectionInterface $sender 
     */
    public function setSender(ConnectionInterface $sender)
    {
        $this->sender = $sender;
    }

    /**
     * Convert json string to object.
     * @param  string $json 
     * @return void
     */
    public function decodeMessage(string $json)
    {
        $message_object = json_decode($json);

        if(! empty($message_object->message))
            $this->message = $message_object->message;

        if(! empty($message_object->data))
            $this->message_data = $message_object->data;
    }

    /**
     * Inicialize routing and call action in controller.
     * @return Mixed
     */
    public function run()
    {
        $route_map = $this->route_map;
        $message = $this->message;
        
        if(empty($route_map[$message]))
            throw new WsException("Route '{$route_map[$message]}' not found. Check your routing.");

        $route = $route_map[$message];

        $controller = new $route['controller']($this->sender);

        $action_name = $route['action'];
        
        if(! method_exists($controller, $action_name)) 
            throw new WsException("Action '$action_name' not found in {$route['controller']}.");

        return call_user_func_array(
            [$controller, $action_name], 
            [$this->message_data]);
    }
}