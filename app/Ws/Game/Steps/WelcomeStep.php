<?php

namespace App\Ws\Game\Steps;

use App\Ws\Game\Steps\BaseStep;
use App\Ws\Game\Steps\Interfaces\Step;

class WelcomeStep extends BaseStep
{
    protected $game, $ws_channel;

    public function getStepName()
    {
        return 'WELCOME';
    }

    public function nextStep()
    {
        return new \App\Ws\Game\Steps\RegisterStep();
    }
}