<?php

namespace App\Ws\Game\Steps;

use App\Ws\Game\Steps\BaseStep;
use App\Ws\Game\Steps\Interfaces\Step;
use App\Ws\WsConnectionManager;
use Illuminate\Support\Facades\App;

class WarningStep extends BaseStep
{
    protected $game, $ws_channel, $game_repositry;

    function __construct()
    {
        $this->game_repositry = App::make('App\Repositories\GameRepository');
    }

    public function getStepName()
    {
        return 'WARNING';
    }

    public function nextStep()
    {
        return new \App\Ws\Game\Steps\QuestionStep();
    }

    public function serverCollback()
    {
        $current_question = $this->game->current_question;

        $this->game = $this->game_repositry->update($this->game->id, [
            'current_question' => ++$current_question
        ]);

        $this->data = [
            'timeout' => config('games.warning_screen_timeout'),
            'current_question' => $this->game->current_question
        ];
    }

    public function panelCallback()
    {
        $this->defaultPanelCallback($this->data);
    }

    public function boardCallback()
    {
        $this->defaultBoardCallback($this->data);
    }

    public function playerCollback()
    {
        $this->defaultPlayersCallback($this->data);
    }
}