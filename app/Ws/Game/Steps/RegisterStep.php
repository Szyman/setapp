<?php

namespace App\Ws\Game\Steps;

use App\Ws\Game\Steps\BaseStep;
use App\Ws\Game\Steps\Interfaces\Step;
use App\Ws\WsMessageSender;

class RegisterStep extends BaseStep
{
    protected $game, $ws_channel;

    public function getStepName()
    {
        return 'REGISTER';
    }

    public function nextStep()
    {
        return new \App\Ws\Game\Steps\TermsStep();
    }
}