<?php

namespace App\Ws\Game\Steps;

use App\Exceptions\GameException;
use App\Ws\WsChannel;
use App\Ws\WsConnectionManager;
use App\Ws\WsMessageSender;

abstract class BaseStep
{
    protected $game, $ws_channel;

    /**
     * Return Step name (uppercase)
     * @return String
     */
    abstract function getStepName();

    /**
     * Get next step class.
     * @return Object
     */
    abstract function nextStep();

    /**
     * Initialize classes and run callback universal for all users.
     * @param  Mixed              $game            
     * @param  WsChannel          $ws_channel
     * @return void
     */
    public function init($game, WsChannel $ws_channel)
    {
        $this->game = $game;
        $this->ws_channel = $ws_channel;
    }

    public function runCallbacks()
    {
    	if(empty($this->game) || empty($this->ws_channel))
    		throw new GameException("You need run 'init()' function first!");

    	$this->serverCollback();

        $this->panelCallback();
        $this->boardCallback();
        $this->playerCollback();
    }

    /**
     * Execute callback for Server.
     * @return void
     */
    public function serverCollback()
    {
        return null;
    }

    /**
     * Execute callback for Panel.
     * @return void
     */
    public function panelCallback()
    {
        $this->defaultPanelCallback();
    }

    /**
     * Execute callback for Board.
     * @return void
     */
    public function boardCallback()
    {
        $this->defaultBoardCallback();
    }

    /**
     * Execute callback for Player Remote control.
     * @return void
     */
    public function playerCollback()
    {
        $this->defaultPlayersCallback();
    }

    /**
     * Get formatted response for client.
     * @param  Mixed $data 
     * @return array
     */
    protected function response($data = null)
    {
        return [
            'next_page' => [
                'view_name' => $this->getStepName(),
                'view_data' => $data
            ]
        ];
    }

    /**
     * Run default callback for Panel.
     * @param  Mixed $data
     * @return void
     */
    protected function defaultPanelCallback($data = null)
    {
        $response = $this->response($data);
        $panel = $this->ws_channel->getPanel();

        if(! empty($response)) {
            foreach ($response as $message => $data) {
                WsMessageSender::send($panel, $message, $data);
            }
        }
    }

    /**
     * Run default callback for Board.
     * @param  Mixed $data
     * @return void
     */
    protected function defaultBoardCallback($data = null)
    {
        $response = $this->response($data);
        $board = $this->ws_channel->getBoard();

        if(! empty($response)) {
            foreach ($response as $message => $data) {
                WsMessageSender::send($board, $message, $data);
            }
        }
    }

    /**
     * Run default callback for Players.
     * @param  Mixed $data
     * @return void
     */
    protected function defaultPlayersCallback($data = null)
    {
        $response = $this->response($data);
        $players = $this->ws_channel->getPlayers();

        if(! empty($players) && ! empty($response)) {
            foreach ($players as $player) {
                foreach ($response as $message => $data) {
                    WsMessageSender::send($player, $message, $data);
                }
            }
        }
    }
}