<?php

namespace App\Ws\Game\Steps;

use App\Ws\Game\Steps\BaseStep;
use App\Ws\Game\Steps\Interfaces\Step;

class TermsStep extends BaseStep
{
    protected $game, $ws_channel;

    public function getStepName()
    {
        return 'TERMS';
    }

    public function nextStep()
    {
        return new \App\Ws\Game\Steps\WarningStep();
    }
}