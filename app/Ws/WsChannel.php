<?php

namespace App\Ws;

class WsChannel
{
	/*
    |--------------------------------------------------------------------------
    | Websocket Channel
    |--------------------------------------------------------------------------
    |
    | Manage open connections in one game.
    | Channel allowed to select panel, board or all players in the game.
    |
    */
   
    /**
     * Filtered connections.
     * @var collection
     */
    private $connections;

    /**
     * Game code
     * @var string
     */
    public $game_code;

    function __construct($connections, $game_code)
    {
        $this->connections = $connections;

        $this->game_code = $game_code;
    }

    /**
     * Get game Panel.
     * @return array
     */
    public function getPanel()
    {
        return $this->connections->where('role', 'PANEL')->first();
    }

    /**
     * Get game Board.
     * @return array
     */
    public function getBoard()
    {
        return $this->connections->where('role', 'BOARD')->first();
    }

    /**
     * Get all players in game.
     * @return collection
     */
    public function getPlayers()
    {
        return $this->connections->where('role', 'PLAYER');
    }
}