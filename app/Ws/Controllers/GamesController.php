<?php

namespace App\Ws\Controllers;

use App\Exceptions\GameException;
use App\Repositories\GameRedisRepository;
use App\Services\GameService;
use App\Ws\WsMessageSender;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class GamesController extends WsController
{
    /*
    |--------------------------------------------------------------------------
    | Game Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling all messages
    | about changing clients views and updating games table in the database.
    | Callbacks for any steps are stored in different classes.
    |
    */

    private $steps, $game_service;

    function __construct($from)
    {
        parent::__construct($from);

        $this->game_service = App::make('App\Services\GameService');

        $this->steps = config('games.steps');
    }

    /**
     * Function move game forward to next step.
     * @return void
     */
    public function moveForward()
    {
        $steps = $this->steps;
        $game_code = $this->ws_channel->game_code;
        $game = $this->game_service->get($game_code);
        $ws_channel = $this->ws_channel;

        // Get current step
        $current_step_name = $game->current_step;

        if(empty($steps[$current_step_name])) 
            throw new GameException("Step name '$current_step_name' not found. Check your configuration in config/games.php");
        
        $current_step = new $steps[$current_step_name];
        $current_step->init($game, $ws_channel);

        // Next step
        $step = $current_step->nextStep();
        $step_name = $step->getStepName();

        // Update game step
        $game = $this->game_service->update($game_code, ['current_step' => $step_name]);

        // Run callbacks
        $step->init($game, $ws_channel);
        $step->runCallbacks();
    }
}