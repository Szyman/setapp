<?php

namespace App\Ws\Controllers;

use App\Http\Controllers\Controller;
use App\Ws\WsConnectionManager;
use Illuminate\Support\Facades\App;

class WsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Websockets Controller
    |--------------------------------------------------------------------------
    |
    | This is default WebSocket controller called by WebSocket Router. 
    | Every controller created to work with web sockets require Websocket 
    | Channel to work. 
    |
    */

    protected $ws_channel;

    function __construct($from)
    {
        $ws_conn_manager = App::make('App\Ws\WsConnectionManager');
        $this->ws_channel = $ws_conn_manager->getChannelforConnection($from);
    }
}