<?php

namespace App\Ws;

use App\Exceptions\GameException;
use App\Ws\WsChannel;
use Illuminate\Support\Facades\Log;
use Ratchet\ConnectionInterface;

class WsConnectionManager
{
    /*
    |--------------------------------------------------------------------------
    | Websocket Connection Manager
    |--------------------------------------------------------------------------
    |
    | Manage open connections. 
    | The manager allowed to select panel, board or all players in the game.
    |
    */

    /**
     * All connections.
     * @var collection
     */
    private $connections;

    function __construct()
    {
        $this->connections = collect();
    }

    /**
     * Create chanell for passed connection.
     * @param  ConnectionInterface $conn 
     * @return WsChannel
     */
    public function getChannelForConnection(ConnectionInterface $conn)
    {
        $connections = $this->connections;

        if(! empty($connections[$conn->resourceId]))
            $connection = $connections[$conn->resourceId];
        
        if(empty($connection['game_code']))
            throw new GameException("Missing 'game_code' value!");
        
        $game_code = $connection['game_code'];

        return $this->getChannel($game_code);
    }

    /**
     * Create chanell for passed game code.
     * @param  string $game_code 
     * @return WsChannel
     */
    public function getChannel($game_code)
    {
        $connections = $this->connections->where('game_code', $game_code);
        return new WsChannel($connections, $game_code);
    }

    /**
     * Add new connection.
     * @param ConnectionInterface $conn
     */
    public function add(ConnectionInterface $conn)
    {
        $conn_id = $conn->resourceId;

        $query = $conn->httpRequest->getUri()->getQuery();
        parse_str($query, $subscription);

        if($subscription['as'] == 'TEST') {
            $this->connections->put($conn_id, [
                'connection' => $conn,
                'user_id' => 'TEST',
                'nickname' => 'TEST',
                'role' => 'TEST',
                'game_code' => 'TEST'
            ]);

            return true;
        }

        $this->connections->put($conn_id, [
            'connection' => $conn,
            'user_id' => $subscription['user'],
            'nickname' => isset($subscription['nickname']) ? strtoupper($subscription['nickname']) : null,
            'role' => strtoupper($subscription['as']),
            'game_code' => $subscription['to']
        ]);

        Log::info('New connection: ' . $conn_id);

        return true;
    }

    /**
     * Remove existing connection.
     * @param  ConnectionInterface $conn [description]
     * @return [type]                    [description]
     */
    public function remove(ConnectionInterface $conn)
    {
        $conn_id = $conn->resourceId;

        if($this->connections->has($conn_id))
            $this->connections->forget($conn_id);

        $conn->close();

        Log::info('Connection close: ' . $conn->resourceId);

        return true;
    }

    /**
     * Return all ws connection in app.
     * @return collection
     */
    public function all()
    {
        return $this->connections;
    }
}