<?php

namespace App\Services;

use App\Repositories\GameRepository;

class GameService
{
    /**
     * The Game repository.
     * @var App\Repositories\GameRepository
     */
    private $game_repository;

    function __construct(GameRepository $game_repository)
    {
        $this->game_repository = $game_repository;
    }

    /**
     * Get the game model.
     * @param  string $game_code
     * @return App\Game
     */
    public function get($game_code)
    {
        $game = $this->game_repository->findWhere(['code' => $game_code])->first();

        return $game;
    }

    /**
     * Get the game entry in correct format.
     * @param  string $game_code 
     * @return stdClass
     */
    public function getParsedEntry($game_code)
    {
        $game = $this->game_repository->with('sheet.questions.answers')->findWhere(['code' => $game_code])->first();

        $quiz = $game->sheet;

        $parsed_quiz = new \stdClass();

        $parsed_quiz->id = $quiz->id;
        $parsed_quiz->user_id = $quiz->user_id;
        $parsed_quiz->name = $quiz->name;
        $parsed_quiz->time = $quiz->time;
        $parsed_quiz->brand = $quiz->brand;

        $parsed_quiz->questions = $quiz->questions->map(function($question) {
       
            $correct = $question->answers->where('is_correct', 1)->first();

            $answers = $question->answers->map(function($answer) {
                    return $answer->text;
                });

            $question_data = [
                'text' => $question->text,
                'image' => $question->image,
                'time' => $question->time,
                'answers' => $answers,
                'correct_answer' => ! empty($correct) ? $correct->position : 0,
            ];

            return $question_data;
        });

        $parsed_game = new \stdClass();

        $parsed_game->id = $game->id;
        $parsed_game->owner = $parsed_quiz->user_id;
        $parsed_game->game_code = $game_code;
        $parsed_game->guestions_number = count($parsed_quiz->questions);
        $parsed_game->current_question = $game->current_question;
        $parsed_game->current_step = $game->current_step;
        $parsed_game->sheet = $parsed_quiz;

        return $parsed_game;
    }

     /**
     * Create a new game entry.
     * @param   $sheet
     * @return App\Game
     */
    public function create(\stdClass $sheet)
    {
        $all_codes = $this->game_repository->all(['code'])->pluck('code');

        $game_code = rand(1000, 9999);

        while ($all_codes->contains($game_code)) {
            $game_code = rand(1000, 9999);
        }

        $data = [
            'sheet_id' => $sheet->id, 
            'code' => $game_code,
            'questions' => count($sheet->questions),
            'current_question' => -1
        ];

        $game = $this->game_repository->findWhere(['sheet_id' => $sheet->id])->first();

        if(empty($game))
            $game = $this->game_repository->create($data);

        return $game;
    }

    /**
     * Update the game.
     * @param  string $game_code
     * @param  array $data      
     * @return App\Game
     */
    public function update($game_code, $data)
    {
        $game = $this->game_repository->updateWhere(['code' => $game_code], $data)->first();
        
        return $game;
    }

    /**
     * Delete the game.
     * @param  string $game_code 
     * @return App\Game
     */
    public function delete($game_code)
    {
        $result = 0;
        $game = $this->game_repository->findWhere(['code' => $game_code])->first();

        if(! empty($game))
            $result = $game->delete();

        return $result;
    }
}