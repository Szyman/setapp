About Project
================
The application is dedicated for Household appliances industry trainers. 
App users may create their own Quizzes and play with participants during training.
This project has not been completed yet. Many features are still not implemented.

About Code
================
This is a part of an larger application based on Laravel 5.5 and Ratchet.
The code included in this repository is a part responsible for Websockets Communication. 
A quiz is divided into few steps. App user may move quiz to next step by sending 'move_forward' message via client panel.

## Connection process
1. Client opens new WebSocket connection with the server
2. Ratchet handles server-site WebSocket
3. Method "onOpen" has been called in App\Ws\WsKernel
4. New connection is added into App\Ws\WsConnectionManager (this class is a singleton)

## Simple message cycle (Client -> Server)
1. App\Ws\WsKernel
2. App\Ws\WsRouter
3. App\Ws\Controllers\WsControllers (e.g GameController)
4. App\Ws\Game\Steps\BaseStep (e.g WelcomeStep)

## Simple message cycle (Server -> Clients)
All server messages are sent by App\Ws\WsMessageSender.

Related projects
================
 - [Laravel framework](https://laravel.com/)
 - [Ratchet](http://socketo.me/)