<?php

return [
	'move_forward' => [
		'controller' => \App\Ws\Controllers\GamesController::class,
		'action' => 'moveForward',
	],

	'my_answer' => [
		'controller' => \App\Ws\Controllers\ResultsController::class,
		'action' => 'saveResult',
	],

	'end_game' => [
		'controller' => \App\Ws\Controllers\GamesController::class,
		'action' => 'endGame',
	],
];