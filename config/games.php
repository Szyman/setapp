<?php

return [
	'players_limit' => 30,

	'warning_screen_timeout' => 5, //sec

	'open_connections_limit' => 5000,

	'steps' => [
		'WELCOME' 			=> \App\Ws\Game\Steps\WelcomeStep::class,
		'REGISTER' 			=> \App\Ws\Game\Steps\RegisterStep::class,
		'TERMS' 			=> \App\Ws\Game\Steps\TermsStep::class,
		'WARNING' 			=> \App\Ws\Game\Steps\WarningStep::class,
		'QUESTION' 			=> \App\Ws\Game\Steps\QuestionStep::class,
		'ANSWERS' 			=> \App\Ws\Game\Steps\AnswersStep::class,
		'RESULTS' 			=> \App\Ws\Game\Steps\ResultsStep::class,
		'QUESTION_RANKING' 	=> \App\Ws\Game\Steps\QuestionRankingStep::class,
		'QUIZ_RANKING' 		=> \App\Ws\Game\Steps\QuizRankingStep::class,
		'FINISH' 			=> \App\Ws\Game\Steps\FinishStep::class,
	],
];